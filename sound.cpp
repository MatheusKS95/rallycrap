/*
RallyCrap
Copyright (C) 2020  Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
THANKS TO LAZYFOO AND JOHN HAMMOND
*/

#include "sound.h"

Sound::Sound()
{

}

bool Sound::play_sfx(const char *soundEffect, Mix_Chunk *chunk)
{
    chunk = Mix_LoadWAV(soundEffect);

    if((Mix_PlayChannel(-1, chunk, 0)) == -1)
    {
        return false;
    }

    return true;
}

bool Sound::play_song(const char *song, Mix_Music *music)
{
    music = Mix_LoadMUS(song);

    if((Mix_PlayMusic(music, -1)) == -1)
    {
        return false;
    }

    return true;
}

void Sound::stop_sfx(Mix_Chunk *chunk)
{
    Mix_FreeChunk(chunk);
    chunk = NULL;
}

void Sound::stop_song(Mix_Music *music)
{
    Mix_FreeMusic(music);
    music = NULL;
}
