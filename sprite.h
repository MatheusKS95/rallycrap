/*
RallyCrap
Copyright (C) 2020  Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
THANKS TO LAZYFOO AND JOHN HAMMOND
*/

#ifndef SPRITE_H
#define SPRITE_H

#ifdef __linux__
#include <SDL2/SDL.h>
#elif __WIN64
#include <SDL.h>
#endif

class Sprite
{
    protected:
        SDL_Surface *surface;
        SDL_Rect rectangle;
        int originX, originY;

    public:
        Sprite(Uint32 colour, int x, int y, int width, int height);
        void draw(SDL_Surface *drawableSurface);
        SDL_Surface* get_surface() const;
        SDL_Rect get_rect();
        bool operator==(const Sprite &aux) const;
};

#endif // SPRITE_H
