# RallyCrap

A simple game made with SDL2.
It's purpose is for myself to learn SDL2, using no engine or other game development framework.
Also, to improve my C++ skills (and general OO and procedural programming skills).

### How to setup?
You'll need Qt Creator, and SDL2, SDL2_image, SDL2_mixer and SDL2_TTF installed in order to build and run this project.
The .pro file already have the settings for Linux machines, but extra work may be required to run on Windows and/or MacOS X.

### Disclaimer
This is a WIP project, created for my own learning purposes. Please keep this in mind. Avoid negative feedback or any kind of insult regarding code quality/organization/whatever.
