/*
RallyCrap
Copyright (C) 2020  Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
THANKS TO LAZYFOO AND JOHN HAMMOND
*/

#ifndef CAR_H
#define CAR_H

#include "sprite.h"

#ifdef __linux__
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#elif __WIN64
#include <SDL.h>
#include <SDL_image.h>
#endif
#include <QMessageBox>

struct movement
{
    bool right = false;
    bool left = false;
    bool up = false;
    bool down = false;
};

class Car : public Sprite
{
    private:
        int PosX, PosY;
        int speed;
        int degrees;
    public:
        Car(Uint32 colour, int x, int y, int width, int height);
        void update_car_props();
        void set_car_pos(int x, int y);
        void set_car_texture();
        void move(movement moves);
        void check_inner_boundary(SDL_Rect rect);
};

#endif // CAR_H
