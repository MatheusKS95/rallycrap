/*
RallyCrap
Copyright (C) 2020  Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
THANKS TO LAZYFOO AND JOHN HAMMOND
*/

#include "rallycrap.h"
#include "ui_rallycrap.h"
#include "constants.h"
#include "sprite.h"
#include "car.h"
#include "sound.h"
#ifdef __WIN64
#include <SDL.h>
#include <SDL_mixer.h>
#elif __linux__
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#endif

RallyCrap::RallyCrap(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::RallyCrap)
{
    ui->setupUi(this);
}

RallyCrap::~RallyCrap()
{
    delete ui;
}

void RallyCrap::on_PlayButton_clicked()
{
    //SDL INITIALIZATION
    if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        SDL_Log("Unable to initialize SDL! %s", SDL_GetError());
        ui->statusBar->showMessage("Unable to initialize SDL", 1000);
        return;
    }

    //SDL_IMAGE INITIALIZATION
    int image_flags = IMG_INIT_PNG;
    int image_init = IMG_Init(image_flags);
    if((image_init&image_flags) != image_flags)
    {
        ui->statusBar->showMessage("Failed to initialize SDL Image");
    }

    //SDL_MIXER INITIALIZATION
    int mixer_flags = MIX_INIT_OGG;
    int init_mix = Mix_Init(mixer_flags);

    if((init_mix&mixer_flags) != mixer_flags)
    {
        ui->statusBar->showMessage("Failed to initialize SDL Mixer");
    }

    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) == -1)
    {
        ui->statusBar->showMessage("Failed to open audio");
    }
    Sound gameSounds;
    Mix_Music *music = NULL;

    //some things that needs to be used everywhere, both across Qt and SDL
    Constants constants;

    //WINDOW CREATION
    SDL_Window *window;
    window = SDL_CreateWindow("RallyCrap", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, constants.WIDTH, constants.HEIGHT, 0);

    if(window == NULL){
        ui->statusBar->showMessage("Failed to display window");
        SDL_Log("Failed to open window. %s", SDL_GetError());
    }

    //SURFACES
    SDL_Surface *screen = SDL_GetWindowSurface(window);

    //Uint32 dirt = SDL_MapRGB(screen->format, 237, 201, 175); older dirt color
    Uint32 dirt = SDL_MapRGB(screen->format, 183, 65, 14);
    //Uint32 grass = SDL_MapRGB(screen->format, 0, 130, 0); older grass color
    Uint32 grass = SDL_MapRGB(screen->format, 34, 139, 34);
    Uint32 black = SDL_MapRGB(screen->format, 0, 0, 0);

    SDL_FillRect(screen, NULL, grass); //background specs

    //outer limits of the track
    Sprite outer_limit(dirt, 30, 30, 530, 330);
    outer_limit.draw(screen);

    //inner limits of the track
    Sprite inner_limit(grass, 90, 90, 430, 230);
    inner_limit.draw(screen);

    //we need a car
    Car car(black, 40, 40, 10, 15);
    car.set_car_texture();
    car.draw(screen);

    //let's have some music
    gameSounds.play_song("JuhaniJunkala_Level 1.ogg", music);

    uint32_t starting_timer;

    SDL_Event event;
    bool playing = true;
    while(playing)
    {
        starting_timer = SDL_GetTicks();
        SDL_UpdateWindowSurface(window);
        if(event.type == SDL_KEYDOWN)
        {
            movement move;
            switch (event.key.keysym.sym) {
                case SDLK_DOWN:
                    move.down = true;
                    break;
                case SDLK_UP:
                    move.up = true;
                    break;
                case SDLK_RIGHT:
                    move.right = true;
                    break;
                case SDLK_LEFT:
                    move.left = true;
                    break;
            }
            car.set_car_texture();
            car.move(move);
            car.check_inner_boundary(inner_limit.get_rect());

            SDL_FillRect( screen, &screen->clip_rect, grass ); //clear everything
            move.up = false;
            move.down = false;
            move.left = false;
            move.right = false;
            outer_limit.draw(screen);
            inner_limit.draw(screen);
            car.draw(screen);
        }
        if(event.type == SDL_KEYUP)
        {
            //printf("\nSTOPPING\n");
        }
        while(SDL_PollEvent(&event))
        {
            if(event.type == SDL_QUIT)
            {
                playing = false;
                break;
            }
        }
        limit_fps(starting_timer);
    }

    IMG_Quit();

    gameSounds.stop_song(music);
    Mix_CloseAudio();
    Mix_Quit();

    SDL_DestroyWindow(window);
    SDL_Quit();
    return;
}

void RallyCrap::on_ExitButton_clicked()
{
    QApplication::quit();
}

void RallyCrap::limit_fps(uint32_t starting_timer)
{
    if((1000 / 60) > SDL_GetTicks() - starting_timer)
    {
        SDL_Delay(1000 / 60 - (SDL_GetTicks() - starting_timer));
    }
}
