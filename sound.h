/*
RallyCrap
Copyright (C) 2020  Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
THANKS TO LAZYFOO AND JOHN HAMMOND
*/

#ifndef SOUND_H
#define SOUND_H

#ifdef __linux__
#include <SDL2/SDL_mixer.h>
#elif __WIN64
#include <SDL_mixer.h>
#endif

class Sound
{
public:
    Sound();
    bool play_sfx(const char soundEffect[] = NULL, Mix_Chunk *chunk = NULL);
    bool play_song(const char song[] = NULL, Mix_Music *music = NULL);
    void stop_sfx(Mix_Chunk *chunk = NULL);
    void stop_song(Mix_Music *music = NULL);
    //void kill_mix();
};

#endif // SOUND_H
