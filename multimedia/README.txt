These files are used by the binary executable, being referenced directly through the code. Please, when compiling the project, copy these files to the directory where the executables are created.

Song: Track 1 from https://opengameart.org/content/5-chiptunes-action created by Juhani Junkala (https://juhanijunkala.com/)

Cars: made by myself
