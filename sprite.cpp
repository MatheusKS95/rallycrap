/*
RallyCrap
Copyright (C) 2020  Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
THANKS TO LAZYFOO AND JOHN HAMMOND
*/

#include "sprite.h"
#include "constants.h"

Sprite::Sprite(Uint32 colour, int x, int y, int width, int height)
{
    Constants constant;
    //surface = SDL_CreateRGBSurface(colour, constant.WIDTH, constant.HEIGHT, 32, 0, 0, 0, 0);
    surface = SDL_CreateRGBSurface(colour, width, height, 32, 0, 0, 0, 0);

    SDL_FillRect(surface, NULL, colour);

    rectangle = surface->clip_rect;

    originX = rectangle.x / 2;
    originY = rectangle.y / 2;

    rectangle.x = x;
    rectangle.y = y;
}

void Sprite::draw(SDL_Surface *drawableSurface)
{
    SDL_BlitSurface(surface, NULL, drawableSurface, &rectangle);
}

SDL_Surface* Sprite::get_surface() const
{
    return surface;
}

SDL_Rect Sprite::get_rect()
{
    return rectangle;
}

bool Sprite::operator==(const Sprite &aux) const
{
    return (surface == aux.get_surface());
}
