/*
RallyCrap
Copyright (C) 2020  Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
THANKS TO LAZYFOO AND JOHN HAMMOND
*/

#include "car.h"
#include "constants.h"

Car::Car(Uint32 colour, int x, int y, int width, int height) : Sprite(colour, x, y, width, height)
{
    degrees = 0;
    speed = 0;
    PosX = PosY = 0;
    update_car_props();
}

void Car::update_car_props()
{
    originX = originY = 0;

    set_car_pos(rectangle.x, rectangle.y);
}

void Car::set_car_pos(int x, int y)
{
    rectangle.x = x - originX;
    rectangle.y = y - originY;
}

void Car::set_car_texture()
{
    SDL_Surface *pngimage = NULL;

    switch (degrees)
    {
        case 0: pngimage = IMG_Load("car.png");
                break;
        case 90: pngimage = IMG_Load("car_right.png");
                 break;
        case 180: pngimage = IMG_Load("car_down.png");
                  break;
        case 270: pngimage = IMG_Load("car_left.png");
                 break;
        default: pngimage = IMG_Load("car.png");
                 break;
    }

    if(pngimage != NULL)
    {
        surface = pngimage;

        int oldX = rectangle.x;
        int oldY = rectangle.y;

        rectangle = surface->clip_rect;

        rectangle.x = oldX;
        rectangle.y = oldY;
        PosX = rectangle.x;
        PosY = rectangle.y;

        update_car_props();
    }
}

void Car::move(movement moves)
{
    Constants constant;

    if(moves.up)
    {
        if(PosY >= 30)
        {
            PosY = PosY - 5;
            degrees = 0;
        }
        else
        {
            QMessageBox::warning(NULL, "Game over", "You went off track and crashed. Try again.");
            PosY = 40;
            PosX = 40;
        }
    }
    else if(moves.down)
    {
        if(PosY <= constant.HEIGHT - 60)
        {
            PosY = PosY + 5;
            degrees = 180;
        }
        else
        {
            QMessageBox::warning(NULL, "Game over", "You went off track and crashed. Try again.");
            PosY = 40;
            PosX = 40;
        }
    }
    else if(moves.left)
    {
        if(PosX >= 30)
        {
            PosX = PosX - 5;
            degrees = 270;
        }
        else
        {
            QMessageBox::warning(NULL, "Game over", "You went off track and crashed. Try again.");
            PosY = 40;
            PosX = 40;
        }
    }
    else if(moves.right)
    {
        if(PosX <= constant.WIDTH - 60)
        {
            PosX = PosX + 5;
            degrees = 90;
        }
        else
        {
            QMessageBox::warning(NULL, "Game over", "You went off track and crashed. Try again.");
            PosY = 40;
            PosX = 40;
        }
    }
    set_car_pos(PosX, PosY);

}

void Car::check_inner_boundary(SDL_Rect rect) //please forgive me
{
    SDL_Point position = {PosX, PosY};
    if(SDL_PointInRect(&position, &rect) == SDL_TRUE)
    {
        QMessageBox::warning(NULL, "Game over", "You went off track and crashed. Try again.");
        PosY = 40;
        PosX = 40;
    }
    set_car_pos(PosX, PosY);
}

