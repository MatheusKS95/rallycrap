/*
RallyCrap
Copyright (C) 2020  Matheus Klein Schaefer

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
THANKS TO LAZYFOO AND JOHN HAMMOND
*/

#ifndef RALLYCRAP_H
#define RALLYCRAP_H

#include <QMainWindow>

namespace Ui {
class RallyCrap;
}

class RallyCrap : public QMainWindow
{
    Q_OBJECT

public:
    explicit RallyCrap(QWidget *parent = nullptr);
    ~RallyCrap();

private slots:
    void on_PlayButton_clicked();

    void on_ExitButton_clicked();

private:
    Ui::RallyCrap *ui;
    void limit_fps(uint32_t starting_timer);
};

#endif // RALLYCRAP_H
